# docker-images-security-lab


## Bienvenidos alumnos

Este repositorio consta de un conjunto de Dockerfiles a los cuales se les puede aplicar mejoras de seguridad.

## Usando el pipeline
Si quieres usar el pipeline con Trivy solo debes ajustar la ruta del dockerfile en el archivo: 

'docker-image-selector.txt'


El pipeline solo tiene activo por los momentos un stage de tests donde solo funciona Trivy.

## Proximante:

- [ ] Hadolint
- [ ] SAST
